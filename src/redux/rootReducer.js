import { combineReducers } from "redux";
import {productReducer as product} from '../redux/reducers/productsReducer';
import { modalReducer as modal} from "../redux/reducers/modalReducer";


export const rootReducer = combineReducers({
    product, modal,
});
