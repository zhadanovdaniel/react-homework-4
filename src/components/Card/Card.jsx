import React, { useState, useEffect } from "react";
import "./Card.scss";

export default function Card(props) {
  const {
    src,
    alt,
    productName,
    productPrice,
    productArticle,
    productColor,
    setActiveStarsCount,
    ModalOpen,
    setActiveArticle,
    activeCart,
    activeStar,
    cartTittle
    //  fav, handleStarClick, setFav
  } = props;


  const [fav, setFav] = useState(false); // состояние горит или не горит

  const handleStarClick = () => {
    const updatedFav = !fav;
    setFav(updatedFav);

    setActiveStarsCount((prevCount) => prevCount + (updatedFav ? 1 : -1));
    // Збереження стану обраного товару в localStorage
    const favorites = JSON.parse(localStorage.getItem("favorites")) || [];

    if (updatedFav) {
      localStorage.setItem(
        "favorites",
        JSON.stringify([...favorites, productArticle])
      );
    } else {
      localStorage.setItem(
        "favorites",
        JSON.stringify(
          favorites.filter((article) => article !== productArticle)
        )
      );
    }
  };

  useEffect(() => {
    // Завантаження стану обраного товару з localStorage
    const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
    setFav(favorites.includes(productArticle));
  }, [productArticle]); // Перезавантаження стану, якщо змінюється артикул товару



  const [cart, setCart] = useState(false);

  useEffect(() => {
    const addedToCart = JSON.parse(localStorage.getItem("addedToCart")) || [];
    setCart(addedToCart.includes(productArticle));
  }, [productArticle]);

  return (
    <div className="card">
      <img src={src} alt={alt} />
      <div>{productName}</div>
      <div>{productPrice} грн</div>
      <div>Артикул: {productArticle}</div>
      <div>Колір: {productColor}</div>
      <div className="card_actions">
         {activeCart && (
        <button
          className={"shoping_cart"}
          onClick={() => {
            ModalOpen();
            setActiveArticle(productArticle);
          }}
        >
          {cartTittle}
        </button>
      )}
      {activeStar &&<div
        className={`star ${fav ? "active" : ""}`}
        onClick={handleStarClick}
      ></div>}
         </div>
     
      
    </div>
  );
}
